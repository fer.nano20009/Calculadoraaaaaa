package p3_tp1;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextField;

public class Operador 
{
	public char SUMA='+';
	public char RESTA='-';
	public char DIVISION='/';
	public char MULTIPLICACION='X';
	private ArrayList<Character> OperandoresSeleccionados;
	private ArrayList<Character> OperandoresUtilizados;
	
	Operador()
	{
		this.OperandoresSeleccionados=new ArrayList<>();
		this.OperandoresUtilizados=new ArrayList<>();
	}
	
		
	
	public void GenerarOperandoresAleatorios( JTextField operador_azar_1, JTextField operador_azar_2) 
	{
		Random random=new Random();
		
		
		
		while (this.OperandoresSeleccionados.size()<2)
		{
			int numeroAleatorio=random.nextInt(4);
			if (numeroAleatorio==0)
			{
				if (!this.OperandoresSeleccionados.contains(SUMA))
					this.OperandoresSeleccionados.add(SUMA);			
			}
			else if (numeroAleatorio==1)
			{
				if (!this.OperandoresSeleccionados.contains(RESTA))
					this.OperandoresSeleccionados.add(RESTA);
			}
			else if (numeroAleatorio==2)
			{
				if (!this.OperandoresSeleccionados.contains(DIVISION))
					this.OperandoresSeleccionados.add(DIVISION);
			}
			else if (numeroAleatorio==3)
			{
				if (!this.OperandoresSeleccionados.contains(MULTIPLICACION))
					this.OperandoresSeleccionados.add(MULTIPLICACION);
			}
		}		
		operador_azar_1.setText(this.OperandoresSeleccionados.get(0).toString());
		operador_azar_2.setText(this.OperandoresSeleccionados.get(1).toString());		
	}
	
	public boolean esOperador(char caracter)
	{
		if (caracter==SUMA)
			return true;
		else if (caracter==RESTA)
			return true;
		else if (caracter==DIVISION)
			return true;		
		else if (caracter==MULTIPLICACION)
			return true;		
		else
			return false;
	}
	
	public boolean contieneEnOperadoresValidosSeleccionados(char c)
	{
		if (this.OperandoresSeleccionados.contains(c))
			return true;
		else
			return false;
	}
	public boolean contieneEnOperadoresValidosUtilizados(char c)
	{
		if (this.OperandoresUtilizados.contains(c))
			return true;
		else
			return false;
		
	}
	public void agregarOperadorUtilizado(char c)
	{
		this.OperandoresUtilizados.add(c);
	}
	
	public int size()
	{
		return this.OperandoresSeleccionados.size();
	}
	
}
