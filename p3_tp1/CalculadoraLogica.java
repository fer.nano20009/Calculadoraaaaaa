package p3_tp1;

import java.util.ArrayList;

public class CalculadoraLogica
{
	
	public int Resultado(String solucion)
	{
		String numero_a="";
		String numero_b="";
		String operador="";
		
		for (int i = 0; i < solucion.length(); i++) 
		{
			//Caso borde en donde se empieza con un signo menos	
			if (i==0 & solucion.charAt(i)=='-')
			{
				numero_a=numero_a+'-';
				i++;
			}			
			if (esNumero(solucion.charAt(i)))
			{				
				if (operador.isEmpty())					
					numero_a=numero_a+solucion.charAt(i);
				else
					numero_b=numero_b+solucion.charAt(i);				
			}
			else
			{
				if (operador.isEmpty())				
					operador=""+solucion.charAt(i);
				else
				{
					numero_a=resolverOperacion(numero_a,numero_b,operador);
					numero_b="";
					operador=""+solucion.charAt(i);
				}				
			}				
		}
		numero_a=resolverOperacion(numero_a,numero_b,operador);
		return Integer.parseInt(numero_a);	
			
	}
	
	private String resolverOperacion(String numero_a, String numero_b, String operador) 
	
	{
		
		int a=Integer.parseInt(numero_a);	
		int b=Integer.parseInt(numero_b);
		
		if (operador.equals("+"))
			return Integer.toString(a+b);
		
		if (operador.equals("-"))
			return Integer.toString(a-b);
		
		if (operador.equals("X"))
			return Integer.toString(a*b);
		
		return Integer.toString(a/b);
		
	}

	public boolean esNumero(Character c)
	{
		if (c=='1' || c=='2' || c=='3' ||c=='4' ||c=='5' ||c=='6' ||c=='7' ||c=='8' ||c=='9' ||c=='0' )
		{
			return true;
		}
		return false;
	}
	
}
